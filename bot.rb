#!/usr/bin/env ruby

# use https://github.com/atipugin/telegram-bot-ruby

require 'net/http'
require 'tmpdir'
require 'telegram/bot'
require 'open3'

def send_message(bot, chat_id, url, message)
  video_url = Open3.popen3("youtube-dl --no-warnings --get-url #{url}")[1].read.chomp.split.first
  text = "Can't send video to Telegram - #{message}, but you can download it by the link: #{video_url}"
  bot.api.send_message(chat_id: chat_id, text: text)
  bot.logger.info(text)
end

Telegram::Bot::Client.run(ENV['TOKEN'], logger: Logger.new($stderr)) do |bot|
  bot.logger.info('Bot has been started')
  bot.listen do |message|
    case message.text
    when '/start', '/?', '/help', 'help'
      bot.api.send_message(chat_id: message.chat.id, \
                          text: "Hello, #{message.from.first_name}. " \
                                "Send me url of the video and i send it to you. " \
                                "Maximum video size is 50 MB. " \
                                "Supported sites: https://rg3.github.io/youtube-dl/supportedsites.html")
    when nil
      bot.api.send_message(chat_id: message.chat.id, text: "not supported")
    else
      url = URI.encode(message.text.chomp)
      if url.match(/^#{URI::regexp}$/)
        tmp_dir = Dir.mktmpdir
        begin
          text = "start video downloading for #{message.from.first_name}"
          bot.logger.info(text)
          #bot.api.send_message(chat_id: message.chat.id, text: text)
          err = Open3.popen3("youtube-dl --youtube-skip-dash-manifest -q --max-filesize 50m " \
                             "-f mp4 --no-warnings -o '#{tmp_dir}/video.mp4' #{url}")[2].read.chomp
          if err.match(/ERROR:/)
            bot.api.send_message(chat_id: message.chat.id, text: err)
            bot.logger.info(err)
          elsif not File.exists?("#{tmp_dir}/video.mp4")
            send_message(bot, message.chat.id, url, "Video size is larger then 50MB")
          else
            text = "start video sending for #{message.from.first_name}"
            bot.logger.info(text)
            #bot.api.send_message(chat_id: message.chat.id, text: text)
            begin
              bot.api.send_video(chat_id: message.chat.id, video: Faraday::UploadIO.new("#{tmp_dir}/video.mp4", "video/mp4"))
            rescue Exception => err
              send_message(bot, message.chat.id, url, err.message)
            end
          end
        rescue Exception => e
          bot.logger.info(e.message)
        ensure
          FileUtils.remove_entry(tmp_dir, true)
        end
      else
        text = "#{url} is not a valid url"
        begin
          bot.api.send_message(chat_id: message.chat.id, text: text)
          bot.logger.info(text)
        rescue Exception => e
          bot.logger.info(e.message)
        end
      end
    end
  end
end
