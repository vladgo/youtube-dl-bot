### Telegram bot @media_dl_bot for download media with youtube-dl.

For deploy, run on ubuntu 16.04 or higher:
```
apt-get update
apt-get install curl -y
curl https://gitlab.com/vladgo/youtube-dl-bot/raw/master/deploy.sh | sh
```
