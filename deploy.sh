#!/bin/bash
set -e

apt-get update
apt-get install ruby ruby-bundler python ffmpeg git cron sudo -y
curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
chmod a+rx /usr/local/bin/youtube-dl
useradd -m bot
sudo -u bot -i git clone https://gitlab.com/vladgo/youtube-dl-bot.git
crontab -l | { cat; echo "@daily youtube-dl -U"; } | crontab -
cd /home/bot/youtube-dl-bot
bundle
cp /home/bot/youtube-dl-bot/telegram-bot.service /etc/systemd/system/media-dl-bot.service
echo "define TOKEN in /etc/systemd/system/media-dl-bot.service"
echo "systemctl daemon-reload"
echo "systemctl start media-dl-bot"
echo "systemctl enable media-dl-bot"
